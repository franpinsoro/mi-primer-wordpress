FROM ubuntu:19.10
LABEL MANTEINER="Francisco Javier Arbues"

ENV DEBIAN_FRONTEND=noninteractive 
RUN apt-get update && \
        apt-get install -y apache2 && \
        apt-get install -y php libapache2-mod-php  php-mysql php-gd && \
        ln -sf /dev/stderr /var/log/apache2/error.log && \
        ln -sf /dev/stdout /var/log/apache2/access.log && \
        echo "<html><body><hr/><h1>Hola Apache/php en Docker</h1> <hr/>" > /var/www/html/info.php && \
        echo " <?php  phpinfo(); ?></body></html>" >> /var/www/html/info.php

ADD https://es.wordpress.org/latest-es_ES.tar.gz /var/www
WORKDIR /var/www
RUN rm -rf html/* && tar -zxvf latest-es_ES.tar.gz && mv wordpress/* html/. && rm -rf wordpress

EXPOSE 80

ENTRYPOINT ["/usr/sbin/apachectl", "-D", "FOREGROUND"]
